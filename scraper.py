# in order to use that code need to have login and passwords
import requests
from threading import Event

url = 'https://rutracker.org/forum/viewtopic.php?t=5885273' # url for scrap

user_agent_val = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36'

session = requests.Session()
r = session.get(url, headers = {
    'User-Agent': user_agent_val
})

session.headers.update({'Referer':url})
session.headers.update({'User-Agent':user_agent_val})

_xsrf = session.cookies.get('_xsrf', domain="rutracker.org")

post_request = session.post(url, {
     'backUrl': 'https://rutracker.org/',
     'username': 'login',
     'password': 'password',
     '_xsrf':_xsrf,
     'remember':'yes',
})

# wait for 10 seconds in order to let browser load page 
Event().wait(10)
session.headers.update({'Referer':url})
session.headers.update({'User-Agent':user_agent_val})

with open("page__scrap.html","w",encoding="utf-8") as i:
    i.write(post_request.text)

